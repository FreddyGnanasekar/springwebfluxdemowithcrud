package com.webflux;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringWebFluxDemoWithCrudApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringWebFluxDemoWithCrudApplication.class, args);
	}

}
